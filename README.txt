
This program demonstrates how it is possible to create pdfs easily with Reportlab.

When you want to create a pdf with Reportlab, the ordinary way is to write a full python program, which is really a complex matter.

Another way is that you create a XML file, with RML syntax (Reportlab MArkup Language), and send it to the Reportlab program for processing. This is overkill for a lot of applications.


A simpler method
================

The method I propose here is to create a template written in TOML.

This template specifies the geometry of the pages, the various styles and fonts used, table style, the list of fixed elements to write (typically the header logo, address, date, invoice number, etc), and the list (story) of flow elements such as paragraphs and a table of items:

    see file /templates/FACTURE_ACME-CH.toml

The names of the TOML items are very close to the names used by Reportlab, so it is easy to understand.


Then, you must write a program that takes your data, and create a dictionary with all the data that you want to output in you pdf.
The stucture is up to you, as the template has placeholders (which are just python format string placeholders) that can access any items in your dictionary.

    for an example, see job_factures.py
    
        the variable doc_data is the dictionary.
        the program calls the method reportlabutils.create_pdf(TOML_TEMPLATE_PATH, doc_data, OUTPUT_PDF_FILENAME)
        which will create a pdf file.
        
All the work of mixing the TOML template and the client_data dictionary, and creating the pdf is done by the module myutils/reportlabutils.py.

The printing of the Swiss Invoice QR-Code is done by myutils/reportlabQRcode.py

If you run the program job_factures.py, the pdf created is /output_pdf/DEV_facture_sample.pdf

If you want to test, you can modify the text of the TOML template and run the program.
The template has text in french (default) and german 'text_de'.
The proper text is chosen, specified by the 'Z_LANG_DOCUMENT' key in client_data.





# Copyright 2023 Nicolas Riesch

import argparse
from datetime import date, datetime
from collections import defaultdict
import os
import sys
import re
import csv
import logging
import myutils.fileutils as fileutils
import myutils.reportlabutils as reportlabutils
import myutils.miscutils as miscutils


#======= global vars and func defs ========

logger = logging.getLogger(__name__)


#================================================
#                    MAIN
#================================================

def main():

    # configure root logger
    
    logfilename = fileutils.get_stem_minimal(__file__) + '.log'   # plain name of the current file with suffix '_STEP...'  removed
    fileutils.configure_root_logger(logfilename=logfilename)      # configure root logger

    
    #=================== start program ===================

    logger.info('')
    logger.info('=== START ===')

    logger.info('load toml config')
    mytomldict = fileutils.load_config_toml(__file__)

    payeur_id                        = '567037'
    payeur_civilite                  = 'M.'
    payeur_prenom                    = 'Jean'
    payeur_nom                       = 'Dupont'
    payeur_societe                   = None
    payeur_service                   = None
    payeur_adr_compl                 = None
    payeur_voie_nom                  = 'rue de St-Jean'
    payeur_voie_no                   = '50 A'
    payeur_boite                     = None
    payeur_distr_compl               = None
    payeur_npa                       = '1225'
    payeur_localite                  = 'Genève'
    payeur_province                  = None
    payeur_pays                      = 'Switzerland'
    payeur_Z_COUNTRYCODE             = 'CH'
    payeur_Z_PAYS_DEST_FOR_ETIQUETTE = None     # null if same as pays postage

    dest_id                        = '829983'
    dest_civilite                  = 'Mme'
    dest_prenom                    = 'Evelyne'
    dest_nom                       = 'Durand'
    dest_societe                   = None
    dest_service                   = None
    dest_adr_compl                 = None
    dest_voie_nom                  = 'rue de la Chouette'
    dest_voie_no                   = '123'
    dest_boite                     = None
    dest_distr_compl               = None
    dest_npa                       = '1445'
    dest_localite                  = 'Martigny'
    dest_province                  = None
    dest_pays                      = 'Switzerland'
    dest_Z_COUNTRYCODE             = 'CH'
    dest_Z_PAYS_DEST_FOR_ETIQUETTE = None     # null if same as pays postage

    doc_data = {
        "Z_LANG_DOCUMENT" :      'fr',   # try another language, like 'de'
        "Z_DATE_DOCUMENT":       date.today(),
        "Z_COUNTRYCODE_POSTAGE": 'CH',
        "Z_ORGANISATION":        'Acme SA',
        "Z_LOGO":                'ACME_LOGO',
        "Z_ENSEIGNE":            'ENSEIGNE_ACME_CH',
        "Z_NUMFACT":             '1234567',
        "Z_REFERENCE":           '123456789012345678901234567',
        "Z_MONNAIE":             'CHF',
        "Z_MONTANT_TOTAL_FINAL": 250.50,
        
        "PAYEUR": {                                  # postal address of the payer
            "ID":             payeur_id,
            "CIVILITE":       payeur_civilite,
            "PRENOM":         payeur_prenom,
            "NOM":            payeur_nom,
            "SOCIETE":        payeur_societe,
            "SERVICE":        payeur_service,
            "ADR_COMPL":      payeur_adr_compl,
            "VOIE_NOM":       payeur_voie_nom,
            "VOIE_NO":        payeur_voie_no,
            "BOITE":          payeur_boite,
            "DISTR_COMPL":    payeur_distr_compl,
            "NPA":            payeur_npa,
            "LOCALITE":       payeur_localite,
            "PROVINCE":       payeur_province,
            "PAYS":           payeur_pays,
            "Z_COUNTRYCODE":  payeur_Z_COUNTRYCODE,
            "Z_PAYS_DEST_FOR_ETIQUETTE":  payeur_Z_PAYS_DEST_FOR_ETIQUETTE,     # null if same as pays postage
            "ADDRESS_FULL_DEST": miscutils.format_reportlab_address(payeur_civilite, payeur_prenom, payeur_nom, payeur_societe, payeur_service, payeur_adr_compl, payeur_voie_nom, payeur_voie_no, payeur_boite, payeur_distr_compl, payeur_npa, payeur_localite, payeur_province, payeur_Z_PAYS_DEST_FOR_ETIQUETTE)
        },
        "DEST": {                                  # postal address of the destinataire
            "ID":             dest_id,
            "CIVILITE":       dest_civilite,
            "PRENOM":         dest_prenom,
            "NOM":            dest_nom,
            "SOCIETE":        dest_societe,
            "SERVICE":        dest_service,
            "ADR_COMPL":      dest_adr_compl,
            "VOIE_NOM":       dest_voie_nom,
            "VOIE_NO":        dest_voie_no,
            "BOITE":          dest_boite,
            "DISTR_COMPL":    dest_distr_compl,
            "NPA":            dest_npa,
            "LOCALITE":       dest_localite,
            "PROVINCE":       dest_province,
            "PAYS":           dest_pays,
            "Z_COUNTRYCODE":  dest_Z_COUNTRYCODE,
            "Z_PAYS_DEST_FOR_ETIQUETTE":  dest_Z_PAYS_DEST_FOR_ETIQUETTE,     # null if same as pays postage
            "ADDRESS_FULL_DEST": miscutils.format_reportlab_address(dest_civilite, dest_prenom, dest_nom, dest_societe, dest_service, dest_adr_compl, dest_voie_nom, dest_voie_no, dest_boite, dest_distr_compl, dest_npa, dest_localite, dest_province, dest_Z_PAYS_DEST_FOR_ETIQUETTE)
        },
        "ARTICLES": [
            {
                "ITEM_NO":         '77889',
                "ITEM_LIBELLE":    'Brosse SuperCurl Extra',
                "QUANTITE":        2,
                "PU":              120.00,
                "Z_MONTANT_FINAL": 240,
            },
            {
                "ITEM_NO":         '73684',
                "ITEM_LIBELLE":    'Laque SuperLaq',
                "QUANTITE":        1,
                "PU":              10.50,
                "Z_MONTANT_FINAL": 10.50,
            }
        ],
    }
    #print(doc_data)
    #print(fileutils.PYAPP_TEMPLATEDIRPATH + '/FACTURE_ACME-CH.toml')
    #print(fileutils.PYAPP_PDFDIRPATH + '/DEV_facture_sample.pdf')
    
    
    # CALL THIS FUNCTION WITH doc_data as argument, TO CREATE A QR-CODE INVOICE FOR SWITZERLAND
    reportlabutils.create_pdf(fileutils.PYAPP_TEMPLATEDIRPATH + '/FACTURE_ACME-CH.toml', doc_data, fileutils.PYAPP_PDFDIRPATH + '/DEV_facture_sample.pdf')




#=== run main ===

if __name__ == '__main__':
    try:
        main()

    except SystemExit:
        logger.info('sys.exit() called')
        raise
    
    except BaseException as e:     # catch all exceptions
        logger.exception(e)
        raise



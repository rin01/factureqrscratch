# Copyright 2023 Nicolas Riesch

import io
import toml
import qrcode
from PIL import Image

from reportlab.lib.units import mm
from reportlab.lib.pagesizes import A4
from reportlab.lib.utils import ImageReader

import myutils.miscutils as miscutils
import myutils.fileutils as fileutils

# les coordonnées sont en mm. Point <0, 0> est le coin inférieur gauche de la page A4.
# ci-desssous, R indique une coordonnée de la partie récépissé, et P pour la partie paiement.

QR_width             = A4[0]
QR_recepisse_width   =  62 * mm
QR_height            = 105 * mm

QX_R_Recepisse       =   7 * mm   # position X du titre "Récépissé"        dans partie récépissé       norme est 5
QX_R_Montant         =  30 * mm   # position X du label "Montant"          dans partie récépissé
QX_R_PointDepot      =  44 * mm   # position X du label "Point de dépôt"   dans partie récépissé
QX_P_SectionPaiement =  67 * mm   # position X du titre "Section paiement" dans partie paiement        norme est 67, également position X du QR-Code
QX_P_Montant         =  83 * mm   # position X du label "Montant"          dans partie paiement
QX_P_Compte          = 118 * mm   # position X du label "Compte"           dans partie paiement        normalement 118

QY_RP_RSPC           =  96 * mm   # position Y du titre "Récépissé", "Section paiement" et du label "Compte"
QY_R_Compte          =  91 * mm   # position Y du label "Compte"           dans la partie récépissé
QY_P_Qrcode          =  42 * mm   # posision Y du QR-Code                                              norme est 42
QY_RP_MonnaieMontant =  35 * mm   # position Y du label "Monnaie" et "Montant"
QY_R_PointDepot      =  22 * mm   # position Y du label "Point de dépôt"   dans la partie récépissé


labels = {'fr': {'recepisse':           'Récépissé',
                 'section_paiement':    'Section paiement',
                 'compte':              'Compte / Payable à',
                 'reference':           'Référence',
                 'info_suppl':          'Informations supplémentaires',
                 'payable_par':         'Payable par',
                 'payable_par_nom_adr': 'Payable par (nom/adresse)',
                 'monnaie':             'Monnaie',
                 'montant':             'Montant',
                 'point_depot':         'Point de dépôt',
                 'a_detacher':          'À détacher avant le versement'
                },
          'de': {'recepisse':           'Empfangsschein',
                 'section_paiement':    'Zahlteil',
                 'compte':              'Konto / Zahlbar an',
                 'reference':           'Referenz',
                 'info_suppl':          'Zusätzliche Informationen',
                 'payable_par':         'Zahlbar durch',
                 'payable_par_nom_adr': 'Zahlbar durch (Name/Adresse)',
                 'monnaie':             'Währung',
                 'montant':             'Betrag',
                 'point_depot':         'Annahmestelle',
                 'a_detacher':          'Vor der Einzahlung abzutrennen'
                },
          'it': {'recepisse':           'Ricevuta',
                 'section_paiement':    'Sezione pagamento',
                 'compte':              'Conto / Pagabile a',
                 'reference':           'Riferimento',
                 'info_suppl':          'Informazioni supplementari',
                 'payable_par':         'Pagabile da',
                 'payable_par_nom_adr': 'Pagabile da (nome/indirizzo)',
                 'monnaie':             'Valuta',
                 'montant':             'Importo',
                 'point_depot':         'Punto di accettazione',
                 'a_detacher':          'Da staccare prima del versamento'
                },
          'en': {'recepisse':           'Receipt',
                 'section_paiement':    'Payment part',
                 'compte':              'Account / Payable to',
                 'reference':           'Reference',
                 'info_suppl':          'Additional information',
                 'payable_par':         'Payable by',
                 'payable_par_nom_adr': 'Payable by (name/address)',
                 'monnaie':             'Currency',
                 'montant':             'Amount',
                 'point_depot':         'Acceptance point',
                 'a_detacher':          'Separate before paying in'
                },
         }


                 
def draw_QRCode(canv, client_data, default_lang):
    canv.saveState()

    # elem1, elem 1r etc sont désignés pour correspondre au schema "Style Guide QR-facture" p. 15
    print(client_data)
    my_lang     = client_data['Z_LANG_DOCUMENT']
    my_enseigne = client_data['Z_ENSEIGNE']

    with open(fileutils.PYAPP_TEMPLATEDIRPATH + '/QRCode_bank_data.toml') as f:
        bank_data = toml.load(f)

    my_labels    = labels.get(my_lang[:2])
    if my_labels is None:
        my_labels = labels[default_lang]
        
    my_bank_data = bank_data[my_enseigne]


    # == compute values to be printed in QR-code ==

    qr_CRE_qr_iban_spaced            = my_bank_data['qr_iban_spaced']
    qr_CRE_qr_iban                   = qr_CRE_qr_iban_spaced.replace(' ', '')
    qr_CRE_nom                       = my_bank_data['nom']
    qr_CRE_voie_nom                  = my_bank_data['voie_nom']
    qr_CRE_voie_no                   = my_bank_data['voie_no']
    qr_CRE_npa                       = my_bank_data['npa']
    qr_CRE_localite                  = my_bank_data['localite']
    qr_CRE_pays                      = my_bank_data['pays']

    qr_CRE_voie_nom_no               = miscutils.concat_coalesce_nzs(' ', qr_CRE_voie_nom, qr_CRE_voie_no)
    qr_CRE_npa_localite              = miscutils.concat_coalesce_nzs(' ', qr_CRE_npa, qr_CRE_localite)

    qr_reference                     = client_data['Z_REFERENCE']
    qr_reference_spaced              = miscutils.format_spaced_QRcode_reference(qr_reference)

    qr_monnaie                       = client_data['Z_MONNAIE']
    qr_montant_total_final_unspaced  = miscutils.format_QRcode_montant_unspaced(client_data['Z_MONTANT_TOTAL_FINAL'])
    qr_montant_total_final_spaced    = miscutils.format_QRcode_montant_spaced(client_data['Z_MONTANT_TOTAL_FINAL'])

    qr_DEBF_line_name = miscutils.concat_coalesce_nzs(' ', client_data['PAYEUR']['PRENOM'], client_data['PAYEUR']['NOM'])
    if qr_DEBF_line_name == '':
        qr_DEBF_line_name = miscutils.nzs(client_data['PAYEUR']['SOCIETE'])

    qr_DEBF_line1 = miscutils.concat_coalesce_nzs(' ', client_data['PAYEUR']['VOIE_NOM'], client_data['PAYEUR']['VOIE_NO'])
    if qr_DEBF_line1 == '':
        qr_DEBF_line1 = miscutils.nzs(client_data['PAYEUR']['DISTR_COMPL'])

    qr_DEBF_line2 = miscutils.concat_coalesce_nzs(' ', client_data['PAYEUR']['NPA'], client_data['PAYEUR']['LOCALITE'])

    qr_DEBF_pays = miscutils.nzs(client_data['PAYEUR']['Z_COUNTRYCODE'])

    qr_info_supplementaire = ''  # TODO client_data["#INFORMATIONS_SUPPLEMENTAIRES"])


    # == SECTION RECEPISSE ==

    currY = QY_RP_RSPC  # Récépissé
    canv.setFont('Helvetica-Bold', 11)
    canv.drawString(QX_R_Recepisse, currY, my_labels['recepisse'])

    currY = QY_R_Compte  # Compte / Payable à
    canv.setFont('Helvetica-Bold', 6)
    canv.drawString(QX_R_Recepisse, currY, my_labels['compte'])

    canv.setFont('Helvetica', 8)  # données créancier

    currY -= 12
    canv.drawString(QX_R_Recepisse, currY, qr_CRE_qr_iban_spaced)

    currY -= 9
    canv.drawString(QX_R_Recepisse, currY, qr_CRE_nom)

    currY -= 9
    canv.drawString(QX_R_Recepisse, currY, qr_CRE_voie_nom_no)

    currY -= 9
    canv.drawString(QX_R_Recepisse, currY, qr_CRE_npa_localite)

    currY -= 17  # Référence
    canv.setFont('Helvetica-Bold', 6)
    canv.drawString(QX_R_Recepisse, currY, my_labels['reference'])

    currY -= 12
    canv.setFont('Helvetica', 8)
    canv.drawString(QX_R_Recepisse, currY, qr_reference_spaced)

    currY -= 17  # Payable par
    canv.setFont('Helvetica-Bold', 6)
    canv.drawString(QX_R_Recepisse, currY, my_labels['payable_par'])

    canv.setFont('Helvetica', 8)

    currY -= 12
    canv.drawString(QX_R_Recepisse, currY, qr_DEBF_line_name)

    currY -= 9
    canv.drawString(QX_R_Recepisse, currY, qr_DEBF_line1)

    currY -= 9
    canv.drawString(QX_R_Recepisse, currY, qr_DEBF_line2)

    currY = QY_RP_MonnaieMontant  # Monnaie  Montant
    canv.setFont('Helvetica-Bold', 6)
    canv.drawString(QX_R_Recepisse, currY, my_labels['monnaie'])
    canv.drawString(QX_R_Montant,   currY, my_labels['montant'])

    currY -= 9
    canv.setFont('Helvetica', 8)
    canv.drawString(QX_R_Recepisse, currY, qr_monnaie)
    canv.drawString(QX_R_Montant,   currY, qr_montant_total_final_spaced)   # espaces pour milliers et 2 décimales

    currY = QY_R_PointDepot
    canv.setFont('Helvetica-Bold', 6)
    canv.drawString(QX_R_PointDepot, currY, my_labels['point_depot'])


    # == SECTION PAIEMENT ==

    currY = QY_RP_RSPC  # Section paiement
    canv.setFont('Helvetica-Bold', 11)
    canv.drawString(QX_P_SectionPaiement, currY, my_labels['section_paiement'])

    canv.setFont('Helvetica-Bold', 8)  # Compte / Payable à
    canv.drawString(QX_P_Compte, currY, my_labels['compte'])

    canv.setFont('Helvetica', 10)  # données créancier

    currY -= 18
    canv.drawString(QX_P_Compte, currY, qr_CRE_qr_iban_spaced)

    currY -= 12
    canv.drawString(QX_P_Compte, currY, qr_CRE_nom)

    currY -= 12
    canv.drawString(QX_P_Compte, currY, qr_CRE_voie_nom_no)

    currY -= 12
    canv.drawString(QX_P_Compte, currY, qr_CRE_npa_localite)

    currY -= 17  # Référence
    canv.setFont('Helvetica-Bold', 8)
    canv.drawString(QX_P_Compte, currY, my_labels['reference'])

    currY -= 12
    canv.setFont('Helvetica', 10)
    canv.drawString(QX_P_Compte, currY, qr_reference_spaced)

    currY -= 17  # Informations supplementaires
    canv.setFont('Helvetica-Bold', 8)
    canv.drawString(QX_P_Compte, currY, my_labels['info_suppl'])

    canv.setFont('Helvetica', 10)

    currY -= 12
    canv.drawString(QX_P_Compte, currY, qr_info_supplementaire)

    currY -= 17  # Payable par
    canv.setFont('Helvetica-Bold', 8)
    canv.drawString(QX_P_Compte, currY, my_labels['payable_par'])

    canv.setFont('Helvetica', 10)

    currY -= 12
    canv.drawString(QX_P_Compte, currY, qr_DEBF_line_name)

    currY -= 12
    canv.drawString(QX_P_Compte, currY, qr_DEBF_line1)

    currY -= 12
    canv.drawString(QX_P_Compte, currY, qr_DEBF_line2)


    currY = QY_RP_MonnaieMontant  # Monnaie  Montant
    canv.setFont('Helvetica-Bold', 8)
    canv.drawString(QX_P_SectionPaiement, currY, my_labels['monnaie'])
    canv.drawString(QX_P_Montant,         currY, my_labels['montant'])

    currY -= 12
    canv.setFont('Helvetica', 10)
    canv.drawString(QX_P_SectionPaiement, currY, qr_monnaie)
    canv.drawString(QX_P_Montant,         currY, qr_montant_total_final_spaced)   # espaces pour milliers et 2 décimales


    # == create QR data ==

    QR_DATA = f"""\
SPC\r\n\
0200\r\n\
1\r\n\
{qr_CRE_qr_iban}\r\n\
S\r\n\
{qr_CRE_nom}\r\n\
{qr_CRE_voie_nom}\r\n\
{qr_CRE_voie_no}\r\n\
{qr_CRE_npa}\r\n\
{qr_CRE_localite}\r\n\
{qr_CRE_pays}\r\n\
\r\n\
\r\n\
\r\n\
\r\n\
\r\n\
\r\n\
\r\n\
{qr_montant_total_final_unspaced}\r\n\
{qr_monnaie}\r\n\
K\r\n\
{qr_DEBF_line_name}\r\n\
{qr_DEBF_line1}\r\n\
{qr_DEBF_line2}\r\n\
\r\n\
\r\n\
{qr_DEBF_pays}\r\n\
QRR\r\n\
{qr_reference}\r\n\
{qr_info_supplementaire}\r\n\
EPD\
"""

    #print(QR_DATA)


    # == draw QR CODE ==

    qr_logo = Image.open(fileutils.PYAPP_LOGODIRPATH +'/CH-Kreuz_7mm.png')

    qr = qrcode.QRCode(version=None,  # make() will determine this automatically
                       error_correction=qrcode.constants.ERROR_CORRECT_M,  # default
                       box_size=10,   # how many pixels each “box” of the QR code is
                       border=0,      # default is 4 box widths, minimal according to the specs. I set to 0, as I dont need margins.
    )
    qr.add_data(QR_DATA)  # number of boxes changes according to size of data to encode. Thus, size in pixels changes also. qr.add_data() encodes string argument in utf-8.
    qr.make()
    qr_img = qr.make_image()  # qrcode.image.pil.PilImage

    qr_logo2 = qr_logo.resize((int(qr_img.size[0] * 7 / 46), int(qr_img.size[1] * 7 / 46)))  # qr code will be printed as 46 mm and logo as 7 mm, pixel ratio must be the same

    qr_img.paste(qr_logo2, ((qr_img.size[0] - qr_logo2.size[0]) // 2, (qr_img.size[1] - qr_logo2.size[1]) // 2))  # paste logo in center of qrcode

    # qr_img.save("X:\\ACCESS Application\\QRFacture\\tmp\qrbof2.png")
    # cnvs.drawImage("X:\\ACCESS Application\\QRFacture\\tmp\qrbof2.png", QX_P_SectionPaiement, QY_P_Qrcode, width=46*mm, height=46*mm)

    with io.BytesIO() as bio:  # BytesIO acts like a file
        qr_img.save(bio, format="png")
        bio.seek(0)  # to reset pointer to start of buffer
        canv.drawImage(ImageReader(bio), QX_P_SectionPaiement, QY_P_Qrcode, width=46 * mm, height=46 * mm)


    # == draw separation dashed lines ==
    
    canv.setStrokeGray(0.3)
    canv.setDash(2, 4)
    canv.setLineWidth(0.3)
    canv.line(10, QR_height, QR_width-10, QR_height)
    canv.line(QR_recepisse_width, 10, QR_recepisse_width, QR_height)
    canv.setDash() #clears it
    
    canv.restoreState()

if __name__ == '__main__':
    pass





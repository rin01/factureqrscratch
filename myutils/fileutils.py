# Copyright 2023 Nicolas Riesch


from pathlib import Path
import re
import logging.config
import toml

# get base directory as string
def get_PYAPP_BASE_DIR():
    dirPath = Path( __file__ ).parent.absolute() # dirpath of current script

    while True:
        flagPath = dirPath / 'PYAPP_BASEDIR_FLAG.txt'
        if flagPath.exists():
            return str(dirPath)    # return string
        dirPath = dirPath.parent

    # PYAPP_BASEDIR_FLAG.txt file not found
    raise Exception('PYAPP_BASEDIR_FLAG.txt not found')


# caller must pass __file__ as argument
# Note: __file__ is always an absolute path
# return minimal stem name of the script as string
#     e.g. 'myscript', 'myscript_STEP002', 'myscript_STEP002_send_mail'  -->  'myscript'
#
# Note: it was not possible to take __name__ (instead of __file__) because it would return '__main__' if the script is run directly with the command python, or python -m
def get_stem_minimal(dunder_file):
    stem = Path(dunder_file).stem                      # plain name as string, without suffix, e.g.     'myscript', 'myscript_STEP002', 'myscript_STEP002_send_mail'
    stem_minimal = re.sub('_STEP[0-9]+.*', '', stem)   # remove _STEP.... from stem
    return stem_minimal


# caller must pass __file__ as argument
# return config toml as dict
def load_config_toml(dunder_file):
    tomlpath = PYAPP_CONFIGDIRPATH + '/' + get_stem_minimal(dunder_file) + '.toml'
    
    with open(tomlpath) as f:
        data = toml.load(f)
        
    return data


# pass named parameter e.g.   logfilename='xxxxxx.log'  to replace the placeholder {logfilename} in base/logging_config.toml
def configure_root_logger(**args):
    tomlpath = PYAPP_CONFIGDIRPATH + '/base/logging_config.toml'

    with open(tomlpath) as f:
        root_logconfig_dict = toml.load(f)

    for k_handlername, dict_handler in root_logconfig_dict['handlers'].items():     # for each handler
        #print('     ------ ', k_handlername)
        for k, v in dict_handler.items():                                               # for each value of the handler dict
            #print('                -- ', k, type(k), v, type(v))
            if isinstance(v, str) and '{' in v:
                v_new = v.format(**args)                                                    # raise KeyError if a placeholder has no replacement parameter
                root_logconfig_dict['handlers'][k_handlername][k] = v_new                   # changing value of dict in-place is allowed
    
    logging.config.dictConfig(root_logconfig_dict)
   
   


# for non string types, the format_spec is applied.
# Then, the resulting string is padded/truncated to the width size.
# This function is used to output strings with specified width size, for text files with fixed columns.
def fixstr_format(a, width, format_spec='', rjustify=False):
    res = None

    if a is None:
        res = ' ' * width
        return res
    
    ss = a
    if type(a) is not str:
        # print('non str  ', type(a))
        ss = format(a, format_spec)
    
    if rjustify:
        res = ss.rjust(width)[:width]  # pad and truncate string to width size
    else:
        res = ss.ljust(width)[:width]  # pad and truncate string to width size

    return res


#=== useful constant paths ===

PYAPP_BASEDIRPATH        = get_PYAPP_BASE_DIR()
PYAPP_CONFIGDIRPATH      = PYAPP_BASEDIRPATH + '/config'
PYAPP_LOGODIRPATH        = PYAPP_BASEDIRPATH + '/ressources/logos'
PYAPP_LOGDIRPATH         = '/var/opt/log'
PYAPP_PDFDIRPATH         = PYAPP_BASEDIRPATH +'/output_pdf'
PYAPP_TEMPLATEDIRPATH    = PYAPP_BASEDIRPATH +'/templates'



# Copyright 2023 Nicolas Riesch


import decimal
from string import Formatter
import html
import re


# the standard "format()" string method raise an error if None is passed as value for a numeric format, and display "None" for string format s.
# The class NoneFormatter formats None as an empty string by default, or any other string passed during initialization.
# e.g. :
#    myformatter = NoneFormatter()
#    res = myformatter.format('hello {:10} {:20} world', 123, 223)
class NoneFormatter(Formatter):
   
    def __init__(self, none_str=''):
        self._none_str = none_str

    def format_field(self, value, format_spec):     # is called for each {...} found in the template string
        if value is None:
            return self._none_str
        
        res = super().format_field(value, format_spec)
        return res

# accept None as argument,
# also escape the characters < > and & of the formatted value.
# and replace \r\n or \n by <br/>.
class NoneEscapehtmlFormatter(Formatter):
   
    def __init__(self, none_str=''):
        self._none_str = none_str

    def format_field(self, value, format_spec):
        if value is None:
            return self._none_str
        
        res = super().format_field(value, format_spec)
        res = html.escape(res, quote=False)
        res = res.replace('\r\n', '<br/>')
        res = res.replace('\n', '<br/>')
        
        return res


# if value is None, return an empty string instead of raising an exception.
def format_nzs(value, format_spec=''):
    if value is None:
        return ''
    
    return format(value, format_spec)


# if val is None, return 0
# NOTE: nzn means non-zero number (Nz is a traditional function in other languages)
def nzn(val):
    if val is None:
        return 0
        
    return val

# if val is None, return '', else return a string
# NOTE: nzs means non-zero string (Nz is a traditional function in other languages)
def nzs(val):
    print('---------------------', val)
    if val is None:
        return ''

    typ = type(val)
    
    if typ is str:
        return val.strip()

    if typ is float or typ is decimal.Decimal:
        return format(val, 'f')  # else, it may be output with exponent notation
    
    return str(val)


# return a string with the concatenated arguments, separated by sep.
# arguments must be None or strings.
def concat_nzs(sep, *args):
    is_first_part = True
    res = ''
    
    for s in args:
        if s is None or s == '' or s.isspace(): # if blank line
            s = ''
                
        if is_first_part:
            res = s.strip()
            is_first_part = False
        else:
            res += sep + s.strip()

    return res.strip()


# return a string with the concatenated arguments, separated by sep.
# arguments must be None or strings.
# if an arg is None or blank string, it is ignored.
# if all arguments are ignored, returns an empty string.
def concat_coalesce_nzs(sep, *args):
    is_first_part = True
    res = ''
    
    for s in args:
        if s is None or s == '': # if blank line, do nothing
                continue
        
        if s.isspace():   # if s is not str, fails
                continue
                
        if is_first_part:
            res = s.strip()
            is_first_part = False
        else:
            res += sep + s.strip()

    return res.strip()


# return an address block, made of non-blank lines, terminated by \n.
def format_reportlab_address(civilite, prenom, nom, societe, service, adr_compl, voie, voie_no, boite, distr_compl, npa, localite, province, pays):
    line_full_name     = concat_coalesce_nzs(' ', civilite, prenom, nom)
    line_societe       = nzs(societe)
    line_service       = nzs(service)
    line_adr_compl     = nzs(adr_compl)
    line_full_voie     = concat_coalesce_nzs(' ', voie, voie_no, boite)
    line_distr_compl   = nzs(distr_compl)
    line_full_localite = concat_coalesce_nzs(' ', npa, localite)
    line_province      = nzs(province)
    line_pays          = nzs(pays)
    
    res = concat_coalesce_nzs('\n', line_full_name, line_societe, line_service, line_adr_compl, line_full_voie, line_distr_compl, line_full_localite, line_province, line_pays)

    return res
    

def format_spaced_QRcode_reference(ref):
    if len(ref) != 27:
        raise Exception("QRCode: reference avec longueur incorrecte '{}'".format(ref))
        
    spaced_ref = ref[0:2] + ' ' + ref[2:7] + ' ' + ref[7:12] + ' ' + ref[12:17] + ' ' + ref[17:22] + ' ' + ref[22:]

    return spaced_ref

# montant is a number.
# return a string with the number formatted with space as group separator and 2 decimal digits. 
def format_QRcode_montant_spaced(montant):
    if montant is None:
        return '0.00'
    
    #print(montant, type(montant), '-----------')
    
    if type(montant) is decimal.Decimal:  # because _ in the format string fails for Decimal type
        montant = float(montant)

    montant_spaced = format(montant, '_.2f').replace('_', ' ')    # replace _ group separator by a space
    
    return montant_spaced
    
# montant is a number.
# return a string with the number formatted with space as group separator and 2 decimal digits. 
def format_QRcode_montant_unspaced(montant):
    if montant is None:
        return '0.00'
    
    #print(montant, type(montant), '-----------')
    
    if type(montant) is decimal.Decimal:  # because _ in the format string fails for Decimal type
        montant = float(montant)

    montant_str = format(montant, '.2f')
    
    return montant_str
    

# skip first non-digit chars, and return an int if any, or None    
def get_int_from_dirty_string(s):
    if s is None or s == '':
        return None

    m = re.match(r'[^0-9]*([0-9]+)', s)
    if m is None:  # no match
        return None
        
    int_str = m.group(1)
    # print('~{}~'.format(int_str))

    if int_str is None or int_str == '':
        return None
    
    return int(int_str)

    
    


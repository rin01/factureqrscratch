# Copyright 2023 Nicolas Riesch


import pandas as pd
import numpy as np
from unidecode import unidecode
import myutils.dbutils as dbutils

# remove accents. transliterate other non-ascii characters, replace / \ ( ) [ ] , ; : . # + - space etc with underscore, and lowercase
# Return a string containing only lowercase a-z0-9_ characters.
# Useful to translate col names in dataframes, so that itertuples() returns namedtuples with valid names.
def asciify(s, to_lowercase=True):
    if s is None:
        return None

    res = s.strip()
    if to_lowercase:
        res = res.lower()
    
    transstr  = r'°/\()[]{}$!,;:.#=+-* '  # translate these chars to underscore
    transstr2 = r'o____________________'  # but ° is translated to 'o' (else, unidecode translates to 'deg')
    transdict = str.maketrans(transstr, transstr2)
    res = res.translate(transdict)

    res = unidecode(res)
    
    charlist = []
    for c in res:
        c_ord = ord(c)
        if (c_ord>=65 and c_ord<=90) or (c_ord>=97 and c_ord<=122) or (c_ord>=48 and c_ord<=57) or (c_ord==95):  # A-Za-z0-9_
            charlist.append(c)
    res = ''.join(charlist)
    
    return res


# col_list is just an iterable, can be a DataFrame
def asciify_columns(col_list, to_lowercase=True):
    res = [asciify(colname, to_lowercase) for colname in col_list]
    return res


# The function SQLServer_insert_dataframe() needs a colmanp, which is a simple dict with key being the target table column names.
# Values can be a string (column name of the source dataframe), or a Colconst, which means the embedded value must be considered as a constant.
# in colmap, a value embedded in an instance of Colconst will be recognized as a constant value, and not as a dataframe column name.
class ColConst:
    def __init__(self, val):
        self.val = val


# insert a dataframe into a SQL Server table.
# colmap is a normal dict, where :
#     keys are target table column names as string (the stringified key list will be passed as the column list of the INSERT statement),
#     values are dataframe column names as string, or a constant value (int, float, string, datetime, etc) embedded in a ColConst instance
#
def sqlserver_insert_dataframe_to_table(df, curs, qname_table, colmap):

    colnames_list         = ['[' + k + ']' for k in colmap]    
    colnames_list_str     = ', '.join(colnames_list)
    placeholders_list_str = ', '.join(['?'] * len(colnames_list))

    sql = 'INSERT INTO ' + qname_table + '(' + colnames_list_str + ') VALUES(' + placeholders_list_str + ')'
    # print(sql)

    i = 0
    for rr in df.itertuples():
        values_list = []

        for v in colmap.values():
            if type(v) is ColConst:
                values_list.append(v.val)
            else:
                assert type(v) is str
                val = getattr(rr, v)
                typval = type(val)
                if pd.isna(val):                  # if NaN, NaT, None, etc
                    values_list.append(None)
                elif typval is np.int64:          # because pyodbc fails for numpy datatypes, as it doesn't know them. np.int64 comes from the datatype 'Int64'.
                    values_list.append(int(val))
                else:
                    values_list.append(val)
                #print( '    ----------->', type(val))
  
        #print(values_list)
        curs.execute(sql, values_list)
        dbutils.sqlserver_purge_cursor(curs)
        i += 1
        #print(i)
        
    return i





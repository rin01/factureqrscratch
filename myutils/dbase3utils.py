# Copyright 2023 Nicolas Riesch


# *** Note installation ***
#
#	On pypi, the version of dbfread is 2.0.7, released Nov 25, 2016. (https://pypi.org/project/dbfread/, en date du 03.08.2022)
#	Sur https://github.com/olemb/dbfread/commits/master, la dernière version est celle du 22.12.2020 (on a dict au lieu de OrderedDict pour les records)
#
#	On ne peut donc pas installer avec :
#	    python -m pip install dbfread
#	car ça installerait une ancienne version.
#	
#   Il faut installer avec :
#	    python -m pip install  --upgrade git+https://github.com/olemb/dbfread.git    (okokok)

# *** Note on dbf file field descriptors ***
#
#   The dbfread code for DBFField StructParser is found in :
#     https://github.com/olemb/dbfread/blob/master/dbfread/dbf.py
#   It is reproduced herebelow :
#   DBFField = StructParser(
#      'DBFField',
#      '<11scLBBHBBBB7sB',
#      ['name',
#       'type',
#       'address',
#       'length',
#       'decimal_count',
#       'reserved1',
#       'workarea_id',
#       'reserved2',
#       'reserved3',
#       'set_fields_flag',
#       'reserved4',
#       'index_field_flag',
#       ])
#   The layout '<BBBBLHHHBBLLLBBH' corresponds to the layout described in :
#     http://ulisse.elettra.trieste.it/services/doc/dbase/DBFstruct.htm
#
#   The field 'decimal_count' is correctly read and usable.
#       (It is missing in the dbfread documentation https://dbfread.readthedocs.io/en/latest/dbf_objects.html#attributes,
#        which only mentions that "Only the name, type and length attributes are used."
#       )
#

# *** Note sur documentation open file ***
#
#   https://dbfread.readthedocs.io/en/latest/introduction.html
#       Loading or iterating over records will open the DBF and memo file once for each iteration.
#       This means the DBF object doesn’t hold any files open, only the RecordIterator object does.
#
#  En fait, il veut dire que dbfread ouvre le fichier dbf et le ferme après avoir lu tous les records (cad, une passe, cad UNE itération qui lit tous les records).
#  Le fichier dbf se ferme donc tout seul.

from dbfread import DBF  # type: ignore
from io import StringIO
import os.path
import pyodbc
from typing import Optional, Union
from collections.abc import Iterable

# dbfpath     : path of the .DBF file
# boolval_YN  : if True, boolean values will be output as 'Y' and 'N'. If False, boolean values will be 1 and 0, which is the default, as tables in SQL Server for DBF bool columns have been specified as INT.
# debug_flag  : print records
# debug_limit : if debug_flag is True, number of records to process. Default is 10. If 0, process all records. If debug_flag is False, this param is ignored.
class MyDBF:
    def __init__(self, dbfpath: str, boolval_YN: bool=False, debug_flag: bool=False, debug_limit: int=10):
        self.dbfpath             = dbfpath
        self.dbfpath_basename    = os.path.basename(dbfpath)
        self.dbf_already_scanned = False  # only one call to write to file or database is allowd

        self.boolval_YN       = boolval_YN
        self.debug_flag       = debug_flag
        self.debug_limit      = debug_limit
        
        #     char_decode_errors='replace'  replaces bad characters with U+FFFD, the official REPLACEMENT CHARACTER
        self.tdbf            = DBF(dbfpath, encoding='cp850', char_decode_errors='replace', recfactory=lambda items: tuple((x[1] for x in items))   )

        self.coldefs         = self.tdbf.fields  # list of DBFFields
        self.colnames        = []                # e.g. "NUMPP"
        self.colfmtstrs      = []                # e.g. "{:s}", "{:.2}" etc
        self.coldbftypes     = []                # e.g. "C", "M" etc
        self.coldbftypes_ext = []                # e.g. "C,10"

        # process output fields format
        for field_def in self.tdbf.fields : # field_def is DBFField
            fld_name          = field_def.name              # e.g. "NUMPP"
            fld_type          = field_def.type              # e.g. "C", "D", etc
            fld_length        = field_def.length
            fld_decimal_count = field_def.decimal_count
            
            self.colnames.append(fld_name)
            self.coldbftypes.append(fld_type)
            
            # see doc at https://dbfread.readthedocs.io/en/latest/field_types.html
            if fld_type == 'C' :                                      # 'C' value is str (also ''). It is never None.
                self.colfmtstrs.append("{:s}")
                self.coldbftypes_ext.append("C,{}".format(fld_length))
            elif fld_type == 'N' :                                    # 'N' is a number :
                if fld_decimal_count == 0 :                               # 'N' value is int if the field has been specified with field decimal_count = 0. Value may be None.
                    self.colfmtstrs.append("{:d}")
                else :                                                    # 'N' value is float if field decimal_count > 0. Value may be None.
                    self.colfmtstrs.append("{{:.{0}f}}".format(fld_decimal_count))  # fixed point notation, e.g. "{:.2}"
                self.coldbftypes_ext.append("N,{},{}".format(fld_length, fld_decimal_count))
            elif fld_type == 'D' :                                    # 'D' value is date, may be None
                self.colfmtstrs.append("{:%Y-%m-%d}")                               # e.g. 2023-02-01
                self.coldbftypes_ext.append("D")
            elif fld_type == 'L' :                                    # 'L' value is bool, may be None
                if self.boolval_YN :
                    self.colfmtstrs.append("{:s}")                                  # boolean values will be output as 'Y' or 'N' strings (default)
                else :
                    self.colfmtstrs.append("{:d}")                                  # boolean values will be output as 0 or 1
                self.coldbftypes_ext.append("L")
            elif fld_type == 'M' :                                    # 'M' (aka memo) value is str, may be None. 
                self.colfmtstrs.append("{:s}")
                self.coldbftypes_ext.append("M")
            else :
                raise Exception("DBF file \"{}\" : type {} unknown".format(self.dbfpath, fld_type))

        assert len(self.colnames)        == len(self.coldefs)
        assert len(self.colfmtstrs)      == len(self.coldefs)
        assert len(self.coldbftypes)     == len(self.coldefs)
        assert len(self.coldbftypes_ext) == len(self.coldefs)


    # check if columns of DBF dbfpath matches with columns of dbfpath_reference
    #   raise DBFStructException if not match
    def check_struct_with(self, dbfpath_reference: str) -> None:

        class DBFStructException(Exception):
            pass
        
        dbfpath_reference_basename = os.path.basename(dbfpath_reference)
        tdbf_ref = DBF(dbfpath_reference, encoding='cp850', char_decode_errors='replace')
        coldefs_ref = tdbf_ref.fields

        if len(self.coldefs) != len(coldefs_ref) :
            raise DBFStructException("ERROR STRUCT {}: diff in number of cols {} != {}".format(self.dbfpath_basename, len(self.coldefs), len(coldefs_ref)))

        for i in range(len(self.coldefs)) :
            if self.coldefs[i].name != coldefs_ref[i].name :
                raise DBFStructException("ERROR STRUCT {}: diff in colname \"{}\" != \"{}\"".format(self.dbfpath_basename, self.coldefs[i].name, coldefs_ref[i].name))
            
            if self.coldefs[i].type != coldefs_ref[i].type : # 'C' 'M' 'N' 'D' 'L' etc
                raise DBFStructException("ERROR STRUCT {}: column {}, diff in col type \"{}\" != \"{}\"".format(self.dbfpath_basename, self.coldefs[i].name, self.coldefs[i].type, coldefs_ref[i].type))

            if self.coldefs[i].length != coldefs_ref[i].length : # we don't care about the length
                print("ERROR STRUCT {}: column {}, diff in col length \"{}\" != \"{}\"".format(self.dbfpath_basename, self.coldefs[i].name, self.coldefs[i].length, coldefs_ref[i].length))

            if self.coldefs[i].decimal_count != coldefs_ref[i].decimal_count :
                raise DBFStructException("ERROR STRUCT {}: column {}, diff in col decimal_count \"{}\" != \"{}\"".format(self.dbfpath_basename, self.coldefs[i].name, self.coldefs[i].decimal_count, coldefs_ref[i].decimal_count))



    # read a dbf file and writes the records in a text file.
    #
    # We don't want carriage returns or linefeed in column values, in the produced text file.
    #     So, firstly, we strip() the string values to remove leading and trailing  \r \n and \t ;
    #     Then, we remove remaining \r characters if any ;
    #     Then, we replace remaining "\n" by the single caret character '^' (to avoid overflow as the replaced string won't be larger than the original one)
    #     Finally, fieldsep sequences will be replaced by a blank character
    #
    # outpath            : rows will be written to this file as csv
    # write_header       : if True, append colnames in first line
    # append_rows        : if True, don't truncate but append rows to the output file
    # fieldsep           : field separator, cannot be the blank character
    # rowterminator      : row terminator
    # append_cols        : if None (default), do nothing. If positive integer, number of fieldseps that will be appended at the end of the lines. If it is a string, append this string as-is.
    # append_cols_header : column names for the append_cols, to write in first line if write_header is True. It must start with the field separator character(s).
    def write_to_file(self, outpath: str, write_header: bool=False, append_rows: bool=False, fieldsep: str="\t", rowterminator: str="\r\n", append_cols: Union[int, str, None]=None, append_cols_header: str='') -> int:
        assert(fieldsep != ' ')

        # check if already used before
        if self.dbf_already_scanned :
            raise Exception("ERROR {}: this MyDBF object has already been used, you should create a new one". format(self.dbfpath))
        self.dbf_already_scanned = True

        # process append_cols argument
        append_cols_str: str = ''
        
        if append_cols is None :
            pass
        elif type(append_cols) is int :
            assert(append_cols > 0)
            append_cols_str = fieldsep * append_cols      # append supplementary ending fieldseps
        elif type(append_cols) is str :
            sep_aux = append_cols[:len(fieldsep)]         # append_cols string should start with fielsep
            if sep_aux != fieldsep :
                raise Exception("ERROR {}: append_cols string starts with \"{}\", should start with fieldsep \"{}\"".format(self.dbfpath, sep_aux, fieldsep))
            append_cols_str = append_cols
        else :
            raise Exception("ERROR {}: bad separator type {}". format(dbfpath, type(append_cols)))

        # process append_cols_header argument
        if write_header and append_cols is not None:
            sep_aux = append_cols_header[:len(fieldsep)]         # append_cols_header string should start with fielsep
            if sep_aux != fieldsep :
                raise Exception("ERROR {}: append_cols_header string starts with \"{}\", should start with fieldsep \"{}\"".format(self.dbfpath, sep_aux, fieldsep))


        # process outpath_append_mode argument
        filemode = 'w'
        if append_rows :
            filemode = 'a'

        # open output file for writing
        with open(outpath, filemode, newline='', encoding="utf-8") as f_out : # newline='' means no translation of end-of-line during writing
            #print(self.colnames)
            #print(self.colfmtstrs)       # e.g. "{:s}", "{:.2}" etc
            #print(self.coldbftypes)      # e.g. "C"
            #print(self.coldbftypes_ext)  # e.g. "C,10"

            # write header if required
            if not append_rows and write_header :
                header_str = fieldsep.join(self.colnames)
                if append_cols_header :
                    header_str += append_cols_header
                f_out.write(header_str + rowterminator)

            # read each record of dbf file
            try :
                rec_count = 0
                for record in self.tdbf :
                    recstr = ""

                    if self.debug_flag and self.debug_limit > 0 :
                        print("-- {} --------".format(rec_count))
                        if rec_count == self.debug_limit :
                            print(" ... ")
                            return rec_count

                    for ii, val in enumerate(record) : # format each column and append it to current line string
                        colname        = self.colnames[ii]
                        colfmtstr      = self.colfmtstrs[ii]
                        coldbftype     = self.coldbftypes[ii]
                        coldbftype_ext = self.coldbftypes_ext[ii]
                        type_val = type(val)
                        if self.debug_flag :
                            print("  {:12s} {:12s} {:12s} {:12s}".format(colname, coldbftype_ext, type_val.__name__, repr(val)))
                    
                        if val is None and coldbftype != 'L' :      # for 'N', 'D', 'M' field datatypes (note: 'C' value is never None. And we don't want bool to be None)
                            val_out = ""    # empty string, will be loaded as NULL into column of any type in SQL Server
                        elif coldbftype == 'C' or coldbftype == 'M' :
                            assert type_val is str
                            val = val.strip()                       # strip \r \n \t blanks etc
                            val = val.replace(fieldsep, ' ')        # if fieldsep found in string, replace by a blank
                            val = val.replace("\r", '')             # discard \r
                            val = val.replace("\n", '^')            # replace embedded \n by a caret
                            val_out = val
                        elif coldbftype == 'L' :
                            assert type_val is bool or val is None
                            if self.boolval_YN :
                                val_out = 'N'
                                if val :
                                    val_out = 'Y'               
                            else :
                                val_out = '0'
                                if val :
                                    val_out = '1'
                        else :
                            val_out = colfmtstr.format(val)     # non-null 'N' or 'D'

                        if ii == 0 :
                            recstr = val_out              # first col
                        else :
                            recstr += fieldsep + val_out  # subsequent cols

                    if append_cols_str is None : # terminates current line
                        recstr += rowterminator
                    else :
                        recstr += append_cols_str + rowterminator

                    rec_count += 1
                    
                    f_out.write(recstr)
                #end for
                        
            except Exception as e :
                raise Exception("ERROR {}: exception raised at record {:d}".format(self.dbfpath, rec_count))
                
            #print('all records have been written out')
        return rec_count

    # insert into a sql table the data, with a sql statement of the kind "INSERT INTO [your qualified table name] VALUES (aa,bb,cc ... uu,vv)"   where aa, bb etc are values from the DBF file.
    # You can have a sql table with more columns, the INSERT statement will ignore them.
    #
    # pyodbc_cursor : autocommit must be False
    # Si un fichier se charge normalement en 10 minutes avec des execute individuels,
    #          avec executemany et fast_executemany, ça prend moins de 2 minutes.
    # pyodbc_cursor       : pyodbc.Cursor (with autocommit == False, which is the default for pyodbc)
    # qualified_tablename : qualified name of the table to insert into
    # append_rows         : if True, don't truncate but append rows to the output file
    # append_cols         : list of values of type None, string, datetime, True, False, int, float, which will be appended as last columns
    # fast_executemany    : argument passed to cursor, for executemany() mode
    # batchsize           : number of rows to insert per executemany() call
    def write_to_database(self, pyodbc_cursor: pyodbc.Cursor, qualified_tablename: str, append_rows: bool=False, append_cols: list=[], fast_executemany: bool=True, batchsize: int=500) :
    
        # truncate table if specified
        if not append_rows :
            sql = "SET NOCOUNT ON; SET XACT_ABORT ON; TRUNCATE TABLE {};".format(qualified_tablename)
            pyodbc_cursor.execute(sql)
            pyodbc_cursor.commit()

        # initialization
        pyodbc_cursor.execute("SET NOCOUNT ON; SET XACT_ABORT ON;")

        dest_recordcount_start: int = pyodbc_cursor.execute("select count(*) from {}".format(qualified_tablename)).fetchval()

        rec_count: int = 0
        batch_values: list[Iterable] = []
        
        nb_columns_full: int = len(self.coldefs) + len(append_cols)

        sql = "INSERT INTO {} VALUES (" + ','.join(['?']*nb_columns_full) + ")"
        sql = sql.format(qualified_tablename)

        pyodbc_cursor.fast_executemany = fast_executemany
        
        try :
            # read each record of dbf file
            for record in self.tdbf :

                if self.debug_flag and self.debug_limit > 0 :
                    if rec_count >= self.debug_limit :
                        break

                values: list = []
                for ii, val in enumerate(record) : # format each column
                    coldbftype     = self.coldbftypes[ii]
                    type_val = type(val)

                    if val is None and coldbftype != 'L' :      # for 'N', 'D', 'M' field datatypes (note: 'C' value is never None. And we don't want bool to be None)
                        values.append(None)
                    elif coldbftype == 'C' or coldbftype == 'M' :
                        assert(type_val is str)
                        val = val.strip()                       # strip \r \n \t blanks etc
                        #val = val.replace(fieldsep, ' ')        # if fieldsep found in string, replace by a blank
                        val = val.replace("\r", '')             # discard \r
                        val = val.replace("\n", '^')            # replace embedded \n by a caret
                        values.append(val)
                    elif coldbftype == 'L' :
                        assert(type_val is bool or val is None)
                        val_out: str|int|None = None
                        if self.boolval_YN :
                            val_out = 'N'
                            if val :
                                val_out = 'Y'               
                        else :
                            val_out = 0
                            if val :
                                val_out = 1
                        values.append(val_out)
                    else :
                        values.append(val)     # non-null 'N' (int or flrat) or 'D' (date)

                if append_cols:
                    values.extend(append_cols)

                batch_values.append(values)

                if len(batch_values) == batchsize :
                    pyodbc_cursor.executemany(sql, batch_values)
                    while pyodbc_cursor.nextset():
                        pass
                    rec_count += batchsize
                    batch_values = []
            #end for

            # insert remaining values if any
            if len(batch_values) > 0 :
                pyodbc_cursor.executemany(sql, batch_values)
                while pyodbc_cursor.nextset():
                    pass
                rec_count += len(batch_values)
                batch_values = []

        except Exception as e :
            raise Exception("ERROR {}: exception raised at record {:d}".format(self.dbfpath, rec_count))

        # and commit
        pyodbc_cursor.commit()
        
        dest_recordcount_end: int = pyodbc_cursor.execute("select count(*) from {}".format(qualified_tablename)).fetchval()

        assert(dest_recordcount_start + rec_count == dest_recordcount_end)

        return rec_count




if __name__ == "__main__" :

    dbfpath           = 'absolute file path to DBF'
    dbfpath_reference = 'absolute file path to reference DBF'
    outpath           = 'absolute file path to output txt file'

    mydbf = MyDBF(dbfpath, debug_flag=False, debug_limit=10)

    mydbf.check_struct_with(dbfpath_reference)

    nb = mydbf.write_to_file(outpath, write_header=False, append_cols="\txx\tyy", append_cols_header="\taaa\tbbb")

    print("lines written in {} : {}".format(outpath, nb))
    

    
    

# Copyright 2023 Nicolas Riesch


import json
import toml

from reportlab.platypus import BaseDocTemplate, PageTemplate, Frame, Paragraph, Spacer, NextPageTemplate, PageBreak, Table, TableStyle
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle, StyleSheet1
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import mm, cm
from reportlab.lib.enums import TA_LEFT, TA_CENTER, TA_RIGHT, TA_JUSTIFY
import reportlab.lib.colors as colors
from reportlab.lib.colors import Color

import myutils.miscutils as miscutils
import myutils.fileutils as fileutils
import myutils.reportlabQRcode as reportlabQRcode



#xx = ParagraphStyle('asdf')
#for k,v in xx.__dict__.items():
#    print(k, v)

#exit(0)


# info utile
#    https://hg.reportlab.com/hg-public/reportlab/file/61ba11e7d143/src/reportlab/lib/styles.py

# NOTE: retours de lignes:
#           - dans drawString("blabla\ncoucou")              \n est affiché comme une fine barre verticale
#           - dans un Paragraph, il faut mettre <br/>, et \n est considéré comme un blanc
#           - dans une table cell, \n est un retour de ligne



def dim2pt(x):
    """Convert a string containing a dimension, e.g. '12 mm', into an int or float value (as pt unit).
    If x is an int, float or None, return it as-is.
    Else, return it as-is.
    """
    if x is None:
        return x

    if type(x) is int:
        return x

    if type(x) is float:
        return x

    if type(x) is str:  # if string
        if x == '*':        # table: all remaining available width
            return x
        if x == 'None':     # table: minimum width to accomodate the column content
            return None
        if x.endswith('mm') :
            x_raw = x[:-2].strip()
            return float(x_raw) * mm
        elif x.endswith('cm') :
            x_raw = x[:-2].strip()
            return float(x_raw) * cm
        elif x.endswith('pt') :
            x_raw = x[:-2].strip()
            if x_raw.isdigit():      # contains only 0..9 chars
                return int(x_raw)
            return float(x_raw)
        else:
            x_raw = x.strip()
            if x_raw.isdigit():      # contains only 0..9 chars
                return int(x_raw)
            return float(x_raw)

    return x

def dims2pts(a):
    """converts a tuple or list of dimensions, e.g. ('12 mm', '100 mm'), into a list of float values (as pt units)"""
    return [dim2pt(x) for x in a]

# for the moment, v must be a string
def getColor_from_val(v):
    mycolor = getattr(colors, v, None)
    if isinstance(mycolor, Color):
        return mycolor
        
    if v.startswith('#'):
        return colors.HexColor(v, 16)

    if v.startswith('Color(') and v.endswith(')'):
        return Color(  *( float(x) for x in v[6:-1].split(','))  )

    raise Exception("'{}' is not a color".format(v))



# lang may be e.g. fr_FR    fr
# Example:  lang == 'en_US', no key 'text_en_US', 'text_en', 'text'
# return None if not found.
def gettextlang(dict_elem, lang, prefix='text'):
    lang_aux = lang
    key = prefix + '_' + lang_aux
    text = dict_elem.get(key)  # lookup language entry as-is
    if text is not None:
        return text
        
    if len(lang_aux) == 5 and lang_aux[2:3] == '_':  # else, try with only first two characters, e.g. 'en', 'de', 'fr' etc
        lang_aux = lang_aux[:2]
        key = prefix + '_' + lang_aux
        text = dict_elem.get(key)
        if text is not None:
            return text
            
    text = dict_elem.get(prefix)  # default
    return text

        
def create_pdf(toml_filepath, client_data, filename_out, title='', author='', subject=''):
    #print(client_data)
    with open(toml_filepath) as f:
        toml_root = toml.load(f)

    #print(json.dumps(toml_root, indent=2))
    # TODO verif si elements de bases existent

    # get language of the document
    lang_document = client_data['Z_LANG_DOCUMENT']
    

    # create the page template with specified geometry
    toml_basedoctemplate = toml_root['BaseDocTemplate']

    toml_pagesize = toml_basedoctemplate['pagesize']
    pagesize = None
    if toml_pagesize == 'A4':
        pagesize = A4
    elif type(pagesize) is list:
        pagesize = dims2pts(toml_pagesize)
    else:
        raise Exception(f'doc template {toml_filepath} est inconnu: BaseDocTemplate.pagesize inconnu, <{toml_pagesize}>')

    showBoundary            = toml_basedoctemplate.get('showBoundary', 0)
    author                  = toml_basedoctemplate.get('author', '')

    # build BaseDocTemplate
    toml_page_template_list = toml_basedoctemplate['pageTemplates']
    page_templates = []
    
    for toml_page_template in toml_page_template_list :  # for each page template
        page_template_id     = toml_page_template['id']
        toml_onpage_funcname = toml_page_template.get('onPage')
        toml_frame_list      = toml_page_template['frames']
        frames = []
        
        for toml_frame in toml_frame_list : # for each frame of this page template
            frame_id         = toml_frame['id']
            frame_x, frame_y = dims2pts(toml_frame['pos'])
            frame_w, frame_h = dims2pts(toml_frame['size'])
            frame = Frame(frame_x, frame_y, frame_w, frame_h, leftPadding=0, bottomPadding=0, rightPadding=0, topPadding=0, id=frame_id)     # default padding is 6 pt
            frames.append(frame)

        page_template = PageTemplate(id=page_template_id, frames=frames)  # onPage=_doNothing,onPageEnd=_doNothing
        if toml_onpage_funcname is not None:
            #pass
            page_template.onPage = on_page_func_factory(toml_root[toml_onpage_funcname], lang_document)
        page_templates.append(page_template)

    doctemplate = BaseDocTemplate(filename_out,
                                  pagesize      = pagesize,
                                  pageTemplates = page_templates,
                                  showBoundary  = showBoundary,        # draw a box around the frame boundaries
                                  # leftMargin,                        # These margins may be overridden by the pageTemplates. They are primarily of interest for the SimpleDocTemplate pour positionner les Frames.
                                  # rightMargin,
                                  # topMargin,
                                  # bottomMargin,
                                  title  = None,  # TODO
                                  author = author)

    # build the paragraph stylesheet
    stylesheet = StyleSheet1()    # see getSampleStyleSheet() in https://hg.reportlab.com/hg-public/reportlab/file/61ba11e7d143/src/reportlab/lib/styles.py
    toml_paragstyle_list = toml_root['paragStyle']
    for toml_paragstyle in toml_paragstyle_list:
        paragstyle_argdict  = normalize_toml_paragstyle(toml_paragstyle, stylesheet, showBoundary)
        #print(paragstyle_argdict)  # TODO  a remettre pour voir
        paragstyle = ParagraphStyle(**paragstyle_argdict)
        stylesheet.add(paragstyle)

    # build the table stylespec       TODO lire si c'est écrit propre et clair
    MAP_OF_TABLESTYLES = {}
    toml_tablestyle_list = toml_root['tableStyle']
    
    TODO_TSTYLE = [                                                                     # TODO: it is now possible to put this style TODO_STYLE as-is in the toml template :-)))
                     # global                                                           #       it doesn't work with this version of toml library, but it works with python 3.11 like a charm
                     # ['GRID',          [0,0],   [-1,-1], 1, 'green'],
                     ['VALIGN',          [0,0],   [-1,-1], 'TOP'],
                     ['LEFTPADDING',     [0,0],   [-1,-1], 1],
                     ['RIGHTPADDING',    [0,0],   [-1,-1], 1],
                     ['TOPPADDING',      [0,0],   [-1,-1], 1],
                     ['BOTTOMPADDING',   [0,0],   [-1,-1], 2],

                     ['LINEBELOW',       [2,-2],  [-1,-2], 1, 'black'],   # line before last line
                     ['TOPPADDING',      [0,-1],  [-1,-1], 2],

                     # header line
                     ['FONT',          [0,0], [-1,0], 'Helvetica-Bold', 10, 12],
                     ['BACKGROUND',    [0,0], [-1,0], '#EEEEEE'],
                     
                     # columns align
                     ['ALIGN', [0,0], [0,-1], 'LEFT'],
                     ['ALIGN', [1,0], [1,-1], 'RIGHT'],
                     ['ALIGN', [2,0], [2,-1], 'RIGHT'],
                     ['ALIGN', [3,0], [3,-1], 'RIGHT'],

                     # bottom line
                     ['FONT',          [0,-1], [-1,-1], 'Helvetica-Oblique', 10, 12],
                    ]

    for toml_tablestyle in toml_tablestyle_list:
        toml_tablestyle_name   = toml_tablestyle['name']
        tblstylelist = TODO_TSTYLE # TODO toml_tablestyle['tstyle']
        tablestyle = TableStyle()
        for tblstyle in tblstylelist:
            print(tblstyle)
            tblstyle_command = tblstyle[0]
            if tblstyle_command in ('GRID', 'BOX', 'INNERGRID', 'LINEABOVE', 'LINEBELOW', 'LINEBEFORE', 'LINEAFTER',):    # cmdname, cellref, cellref, width_pt, color
                tblstyle_color = getColor_from_val(tblstyle[4])
                tablestyle.add(tblstyle_command, tblstyle[1], tblstyle[2], tblstyle[3], tblstyle_color)
            elif tblstyle_command in ('ALIGN',):        # cmdname, cellref, cellref, alignment (LEFT, RIGHT, CENTRE or CENTER, DECIMAL)
                tablestyle.add(tblstyle_command, tblstyle[1], tblstyle[2], tblstyle[3])
            elif tblstyle_command in ('VALIGN',):       # cmdname, cellref, cellref, alignment (TOP, MIDDLE or the default BOTTOM)
                tablestyle.add(tblstyle_command, tblstyle[1], tblstyle[2], tblstyle[3])
            elif tblstyle_command in ('FONT',):         # cmdname, cellref, cellref, fontname, fontsize, leading
                tablestyle.add(tblstyle_command, tblstyle[1], tblstyle[2], tblstyle[3], tblstyle[4], tblstyle[5])
            elif tblstyle_command in ('FONTNAME',):     # cmdname, cellref, cellref, fontname
                tablestyle.add(tblstyle_command, tblstyle[1], tblstyle[2], tblstyle[3])
            elif tblstyle_command in ('ROWBACKGROUNDS',):    # cmdname, cellref, cellref, list of colors
                color_list = [getColor_from_val(s) for s in tblstyle[3]]
                tablestyle.add(tblstyle_command, tblstyle[1], tblstyle[2], color_list)
            elif tblstyle_command in ('TEXTCOLOR', 'BACKGROUND',):    # cmdname, cellref, cellref, color
                tblstyle_color = getColor_from_val(tblstyle[3])
                tablestyle.add(tblstyle_command, tblstyle[1], tblstyle[2], tblstyle_color)
            elif tblstyle_command in ('FONTSIZE', 'LEADING', 'LEFTPADDING', 'RIGHTPADDING', 'TOPPADDING', 'BOTTOMPADDING',):    # cmdname, cellref, cellref, pt
                tablestyle.add(tblstyle_command, tblstyle[1], tblstyle[2], tblstyle[3])
            else:
                raise Exception("table style command unknown {}".format(tblstyle_command))   

        MAP_OF_TABLESTYLES[toml_tablestyle_name] = tablestyle


    toml_globals = toml_root['globals']
    doctemplate.myStylesheet        = stylesheet
    doctemplate.lang_document       = lang_document
    doctemplate.text_default_lang   = toml_globals['text_default_lang']
    doctemplate.none_formatter      = miscutils.NoneFormatter()            # formatter for table cells and drawString
    doctemplate.esc_formatter       = miscutils.NoneEscapehtmlFormatter()  # formatter for Paragraphs
    doctemplate.client_data         = client_data

    # print the story
    story=[]
    toml_story = toml_root['story']
    for toml_element in toml_story:
        if toml_element['type'] == 'NextPageTemplate':
            story.append(NextPageTemplate(toml_element['next_page_template']))

        elif toml_element['type'] == 'Paragraph':
            text      = gettextlang(toml_element, doctemplate.lang_document)
            text_pure = doctemplate.esc_formatter.format(text, client_data)  # text template may contain html tags, but when formatting, we must escape < > and & of client data
            #print(text_pure)
            story.append(Paragraph(text_pure, stylesheet[toml_element['style']]))

        elif toml_element['type'] == 'Spacer':
            story.append(Spacer(1, dim2pt(toml_element['height'])))

        elif toml_element['type'] == 'Table':
            args = {}
            if 'colWidths' in toml_element:
                args['colWidths'] = dims2pts(toml_element['colWidths'])      # list of column width
            if 'rowHeights' in toml_element:
                args['rowHeights'] = dims2pts(toml_element['rowHeights'])
            if 'splitByRow' in toml_element:
                args['splitByRow'] = toml_element['splitByRow']
            if 'repeatRows' in toml_element:
                args['repeatRows'] = toml_element['repeatRows']
            if 'repeatCols' in toml_element:
                args['repeatCols'] = toml_element['repeatCols']
            if 'rowSplitRange' in toml_element:
                args['rowSplitRange'] = toml_element['rowSplitRange']
            if 'spaceBefore' in toml_element:
                args['spaceBefore'] = dim2pt(toml_element['spaceBefore'])    # space before table
            if 'spaceAfter' in toml_element:
                args['spaceAfter'] = dim2pt(toml_element['spaceAfter'])      # space after table
            if 'cornerRadii' in toml_element:
                args['cornerRadii'] = dim2pt(toml_element['cornerRadii'])
            
            
            tbl_head_libelles = gettextlang(toml_element, doctemplate.lang_document, prefix='head_libelles')

            tbl_tail_texts    = gettextlang(toml_element, doctemplate.lang_document, prefix='tail_texts')

            table_data = []
            if tbl_head_libelles is not None:
                rowcells = []
                for cell_text in tbl_head_libelles:
                    cell_text_pure = doctemplate.none_formatter.format(cell_text, client_data)
                    rowcells.append(cell_text_pure)
                table_data.append(rowcells)
            
            tbl_row_paragraph_set = set(toml_element['row_paragraph_list'])
            print(tbl_row_paragraph_set)
            
            tbl_row_texts = gettextlang(toml_element, doctemplate.lang_document, prefix='row_texts')
            tbl_rowlist_element_name = toml_element['rowlist_element_name']
            tbl_rowlist = client_data[tbl_rowlist_element_name]
            for row_detaildict in tbl_rowlist:  # for each row
                rowcells = []
                for j, cell_text in enumerate(tbl_row_texts):
                    if j in tbl_row_paragraph_set:
                        cell_text_html = doctemplate.esc_formatter.format(cell_text, row=row_detaildict)
                        rowcells.append(Paragraph(cell_text_html))
                    else:
                        cell_text_pure = doctemplate.none_formatter.format(cell_text, row=row_detaildict)
                        rowcells.append(cell_text_pure)
                table_data.append(rowcells)

            if tbl_tail_texts is not None:
                rowcells = []
                for cell_text in tbl_tail_texts:
                    cell_text_pure = doctemplate.none_formatter.format(cell_text, client_data)
                    rowcells.append(cell_text_pure)
                table_data.append(rowcells)

            
            #if 'data' in toml_element:
            #    data = toml_element['data']
            #print(data)
            #print(args)
            t = Table(table_data, **args)

            this_table_stylename = toml_element['style']
            this_table_style = MAP_OF_TABLESTYLES[this_table_stylename]
            t.setStyle(this_table_style)
            story.append(t)

        elif toml_element['type'] == 'PageBreak':
            story.append(PageBreak())

    doctemplate.build(story)

    
def on_page_func_factory(toml_command_list, lang_document):
    #print(toml_command_list)
    commands = []

    for toml_command in toml_command_list:
        command_type = toml_command['type']
    
        if command_type == 'canvasString':
            x=dim2pt(toml_command['pos'][0])
            y=dim2pt(toml_command['pos'][1])
            text = gettextlang(toml_command, lang_document)
            commands.append(('canvasString', x, y, text))

        elif command_type == 'canvasSetFont':
            fontname = toml_command['fontName']
            fontsize = toml_command['fontSize']
            commands.append(('canvasSetFont', fontname, fontsize))
            
        elif command_type == 'canvasSetLineWidth':
            width = toml_command['width']
            commands.append(('canvasSetLineWidth', width))
      
        elif command_type == 'canvasLine':
            pos0 = dims2pts(toml_command['pos0'])
            pos1 = dims2pts(toml_command['pos1'])
            commands.append(('canvasLine', pos0, pos1))

        elif command_type == 'canvasParagraph':
            pos   = dims2pts(toml_command['pos'])
            size  = dims2pts(toml_command['size'])
            text  = toml_command['text']
            style = toml_command['style']
            commands.append(('canvasParagraph', pos, size, text, style))
            
        elif command_type == 'canvasQRCode':
            commands.append(('canvasQRCode',))

        elif command_type == 'canvasLogo':
            x=dim2pt(toml_command['pos'][0])
            y=dim2pt(toml_command['pos'][1])
            w=dim2pt(toml_command['size'][0])
            h=dim2pt(toml_command['size'][1])
            commands.append(('canvasLogo', x, y, w, h,))

        elif command_type == 'pass':
            pass
            
        else:
            raise Exception('commande de canvas inconnue dans le template: {}'.format(command_type))

    def on_page_func(canv, document): # inner function
        canv.saveState()
        
        for cmd in commands:
            command_type = cmd[0]
            
            if command_type == 'canvasString' or command_type =='canvasDrawString':
                print('xxxxxxxxxxxxxxxxxx', cmd[3])
                text = document.none_formatter.format(cmd[3], document.client_data)
                canv.drawString(cmd[1], cmd[2], text)
                
            elif command_type == 'canvasDrawRightString':
                text = document.none_formatter.format(cmd[3], document.client_data)
                canv.drawRightString(cmd[1], cmd[2], text)
                
            elif command_type == 'canvasDrawCentredString':
                text = document.none_formatter.format(cmd[3], document.client_data)
                canv.drawCentredString(cmd[1], cmd[2], text)
                
            elif command_type == 'canvasDrawAlignedString':
                text = document.none_formatter.format(cmd[3], document.client_data)
                canv.canvasDrawAlignedString(cmd[1], cmd[2], text)
                
            elif command_type == 'canvasSetFont':
                canv.setFont(cmd[1], cmd[2])
                
            elif command_type == 'canvasSetLineWidth':
                canv.setLineWidth(cmd[1])
                
            elif command_type == 'canvasLine':
                canv.line(cmd[1][0], cmd[1][1], cmd[2][0], cmd[2][1])
                
            elif command_type == 'canvasParagraph':
                text_html = document.esc_formatter.format(cmd[3], document.client_data)
                parag = Paragraph(text_html, document.myStylesheet[cmd[4]])
                w,h = parag.wrapOn(canv, *cmd[2])   # reportlab User Guide uses 'wrap', Driscoll uses 'wrapOn', hard to say ...
                parag.drawOn(canv, cmd[1][0], cmd[1][1]-h)
                
            elif command_type == 'canvasQRCode':
                reportlabQRcode.draw_QRCode(canv, document.client_data, document.text_default_lang) # draw the QR-code bulletin

            elif command_type == 'canvasLogo':
                image_path = '{logodir}/{logoname}.jpg'.format(logodir=fileutils.PYAPP_LOGODIRPATH, logoname=document.client_data['Z_LOGO'])
                print(image_path)
                canv.drawImage(image_path, cmd[1], cmd[2], cmd[3], cmd[4], anchor='sw', preserveAspectRatio=True, showBoundary=False)
                
            else:
                raise Exception('commande de canvas inconnue dans le template: {}'.format(command_type))
                
        canv.restoreState()
        
    return on_page_func


def normalize_toml_paragstyle(d, stylesheet, show_boundary):
    """the attribute 'parent' is kept as string"""
    
    dd = {}

    for k, v in d.items():
        if k in ['name', 'fontName', 'bulletFontName', 'wordWrap', 'bulletAnchor', 'textTransform', 'endDots', 'hyphenationLang']:  # str
            dd[k] = v
        elif k in ['allowWidows', 'allowOrphans', 'splitLongWords', 'justifyLastLine', 'justifyBreaks', 'linkUnderline', 'embeddedHyphenation']:  # int
            dd[k] = v
        elif k in ['spaceShrinkage', 'uriWasteReduce']:  # float
            dd[k] = v
        elif k in['underlineOffset', 'strikeOffset']:    # str, e.g. '0.5*F'
            dd[k] = v
        elif k in ['fontSize', 'leading', 'leftIndent', 'rightIndent', 'firstLineIndent', 'spaceBefore', 'spaceAfter', 'bulletFontSize', 'bulletIndent', 'borderWidth', 'borderRadius', 'underlineWidth', 'strikeWidth', 'underlineGap', 'strikeGap']:
            dd[k] = dim2pt(v)
        elif k in ['bulletColor', 'textColor', 'backColor', 'borderColor', 'underlineColor', 'strikeColor']:  # e.g.   '#F04488'    Color(0,0,0,1)   black     # TODO
            if type(v) is str:
                dd[k] = getColor_from_val(v)
            else:
                raise Exception('ere')
        elif k in ['alignment']:
            if v == 'TA_LEFT':
                dd[k] = TA_LEFT
            elif v == 'TA_CENTER' or v == 'TA_CENTRE':
                dd[k] = TA_CENTER
            elif v == 'TA_RIGHT':
                dd[k] = TA_RIGHT
            elif v == 'TA_JUSTIFY':
                dd[k] = TA_JUSTIFY
            else:
                dd[k] = v
        elif k in ['borderPadding']:
            if type(v) is list:
                dd[k] = dims2pts(v)
            else:
                dd[k] = dim2pt(v)
        elif k == 'parent':
            dd[k] = stylesheet[v]  # lookup ParagraphStyle object of the parent
        else:
            raise Exception("'{}' is not a paragraph style property".format(k))
    
    if show_boundary:
        dd['borderColor'] = colors.red
        dd['borderWidth'] = 0.2
        
    return dd

